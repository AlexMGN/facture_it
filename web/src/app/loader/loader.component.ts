import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    /*const token = localStorage.getItem('access_token');

    if (!token) {
      this.router.navigate(['/login']);
    }*/

    setTimeout(() => {
      this.router.navigate(['/dashboard']);
    }, 3000);
  }

}
