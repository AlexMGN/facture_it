import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '../services/toast/toast.service';
import { UtilsService } from '../services/api/utils.service';
import { LoginService } from '../services/login/login.service';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  test: Date = new Date();
  closeResult = '';
  countries: any;
  google: any;

  newsletterCredentials = {
    email: '',
    agree: false,
  };

  loginCredentials = {
    email: '',
    password: ''
  };

  registerCredentials = {
    email: '',
    password: '',
    address: '',
    ville: '',
    code_postal: '',
    pays: ''
  };

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private modalService: NgbModal, public toast: ToastService, private utils: UtilsService, private auth: LoginService,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.utils.getCountries().subscribe(countries => {
      this.countries = countries;
    });
  }

  newsletter() {
    if (this.newsletterCredentials.email.trim() === '' || !this.newsletterCredentials.agree) {
      console.log('Pas d\'inscription à la newsletter possible. Données manquantes');
    } else {
      console.log('Inscription possible');
      console.log(this.newsletterCredentials);
    }
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any) {
    return;
  }

  login() {
    if (this.validForm(this.loginCredentials, 'login')) {
      this.http.post(environment.api + '/api/user/connect', {
        email: this.loginCredentials.email,
        password: this.loginCredentials.password
      }).subscribe((result: any) => {
        if (!result.token) {
          this.toast.sendErrorToast('Erreur survenue');
        } else {
          localStorage.setItem('access_token', result.token);
          this.modalService.dismissAll();
          this.router.navigate(['/dashboard']);
        }
      });
    }
  }

  registerUser() {
    if (this.validForm(this.registerCredentials, 'register')) {
      localStorage.setItem('access_token', '0fef2efzf43fzfrzrf');
      // Appeller ou Créer un service "auth" qui se chargera des call api connexion/inscription
      this.modalService.dismissAll();
      this.router.navigate(['/dashboard']);
    }
  }

  validForm(form, service) {
    if (service === 'login') {
      return (form.email.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Email') :
        (form.password.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Mot de passe') : true;
    } else if (service === 'register') {
      return (form.email.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Email') :
        (form.password.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Mot de passe') :
          (form.address.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Adresse') :
            (form.ville.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Ville') :
              (form.code_postal.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Code postal') :
                (form.pays.trim() === '') ? this.toast.sendToastForEmptyOrMissingData('Pays') : true;
    }
  }

  async googleAuth() {
    await this.auth.GoogleAuth().then(data => {
      this.google = data;
    }).catch(() => {
      this.toast.sendErrorToast('Une erreur est survenue');
    });

    this.http.post(environment.api + '/api/user/googleConnect', {
      email: this.google.userInfos.email,
      id: this.google.userInfos.id
    }).subscribe((res: any) => {
      localStorage.setItem('access_token', res.token);
      this.modalService.dismissAll();
      this.router.navigate(['/dashboard']);
    });
  }

}
