import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoaderComponent } from './loader/loader.component';
import { LandingComponent } from './landing/landing.component';
import { BankComponent } from './profil/bank/bank.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: () => import('./layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
  }]},
  {
    path: 'loader',
    component: LoaderComponent
  },
  {
    path: 'facture-it',
    component: LandingComponent
  },
  {
    path: 'me/bank',
    component: BankComponent
  },
  {
    path: '**',
    redirectTo: 'facture-it'
  }
  // Pour le path ** il faudra rediriger vers une page 404
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
