import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoaderComponent } from './loader/loader.component';
import { LandingComponent } from './landing/landing.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BankComponent } from './profil/bank/bank.component';
import {NgxPaginationModule} from "ngx-pagination";
import {NgxUiLoaderModule} from "ngx-ui-loader";

const config = {
  apiKey: 'AIzaSyBhZ_VypwhzWyHjQglwmxIx2BYK2yj-1m0',
  authDomain: 'facture-it.firebaseapp.com',
  databaseURL: 'https://facture-it.firebaseio.com',
  projectId: 'facture-it',
  storageBucket: 'facture-it.appspot.com',
  messagingSenderId: '723908828305',
  appId: '1:723908828305:web:bd0e7a54ade76f3040f330'
};

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        NgbModule,
        AngularFireModule.initializeApp(config),
        AngularFireAuthModule,
        ToastrModule.forRoot(),
        NgxPaginationModule,
        NgxUiLoaderModule,
    ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoaderComponent,
    LandingComponent,
    BankComponent

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
