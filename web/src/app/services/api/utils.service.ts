import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private http: HttpClient) { }

  getCountries() {
    return this.http.get('https://restcountries.eu/rest/v2/all');
  }

  isAuth() {
    return this.http.get(environment.api + '/api/auth/user', {
      headers: {
        'Authorization': localStorage.getItem('access_token')
      }
    });
  }
}
