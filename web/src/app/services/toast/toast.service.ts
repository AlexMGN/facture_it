import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastr: ToastrService) { }

  sendToastForEmptyOrMissingData(input) {
    this.toastr.warning('Ce champs ne peut pas être vide', input, {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-center'
    });
  }

  sendSuccessToast(message) {
    this.toastr.success('Félicitations !', message, {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-center'
    });
  }

  sendErrorToast(message) {
    this.toastr.error('Une erreur est survenue !', message, {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-center'
    });
  }
}
