import { Component, OnInit } from '@angular/core';
import {UtilsService} from "../../services/api/utils.service";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {ToastService} from "../../services/toast/toast.service";
import {Router} from "@angular/router";
import {NgxUiLoaderComponent} from "ngx-ui-loader/lib/core/ngx-ui-loader.component";
import {NgxUiLoaderService} from "ngx-ui-loader";

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit {
  page = 1;
  count = 0;
  tableSize = 10;
  tableSizes = [3, 6, 9, 12];

  searchVal = '';

  transactions = [];

  haveAccount = false;
  haveMidAccount = false;

  constructor(private API: UtilsService, private http: HttpClient, private toast: ToastService, private router: Router,
              private ngxLoader: NgxUiLoaderService) { }

  ngOnInit(): void {
    this.API.isAuth().subscribe((res: any) => {
      if (res.user.bank_action === 'disconnected') {
        this.haveAccount = false;
      } else if (!res.user.bankin_uid) {
        this.haveAccount = false;
      } else {
        if (!res.user.bank_connected_item) {
          this.haveAccount = false;
        } else if (res.user.bank_connected_item && res.user.bank_action === 'connected') {
          this.haveAccount = true;
          this.getTransactions();
        }
      }
    }, err => {
      console.log(err);
    });
  }

  onTableDataChange(event) {
    this.page = event;
  }

  checkSearchVal() {
    const filteredTransactions = [];

    // tslint:disable-next-line:triple-equals
    if (this.searchVal && this.searchVal != '') {
      if (this.transactions.length <= 0) {
        this.getTransactions();
      } else {
        for (const selectedTransaction of this.transactions) {
          // tslint:disable-next-line:triple-equals
          if (selectedTransaction.status.toLowerCase().search(this.searchVal.toLowerCase()) != -1 ||
            // tslint:disable-next-line:triple-equals
            selectedTransaction.resource_type.toLowerCase().search(this.searchVal.toLowerCase()) != -1 ||
            // tslint:disable-next-line:triple-equals
            selectedTransaction.date.toLowerCase().search(this.searchVal.toLowerCase()) != -1 ||
            // tslint:disable-next-line:triple-equals
            selectedTransaction.description.toLowerCase().search(this.searchVal.toLowerCase()) != -1 ||
            // tslint:disable-next-line:triple-equals
            selectedTransaction.amount.toLowerCase().search(this.searchVal.toLowerCase()) != -1) {
            filteredTransactions.push(selectedTransaction);
          }
        }
      }

      this.transactions = filteredTransactions.slice();
    } else {
      this.transactions = [];
      this.getTransactions();
    }
  }

  getTransactions = () => {
    this.http.get(environment.api + '/api/auth/openbanking/transactions', {
      headers: {
        'Authorization': localStorage.getItem('access_token')
      }
    }).subscribe((result: any) => {
      this.haveMidAccount = false;
      this.haveAccount = true;

      this.transactions = result.transactions;
    }, error => {
      this.toast.sendErrorToast(error.error.err);
    });
  }

  connectBankAccount = () => {
    this.http.post(environment.api + '/api/auth/openbanking/create/account', {}, {
      headers: {
        'Authorization': localStorage.getItem('access_token')
      }
    }).subscribe((result: any) => {
      window.open(
        result.userBanksURL,
        '_blank'
      );
      this.ngxLoader.start();
    }, error => {
      this.toast.sendErrorToast(error.error.err);
    });
  }
}
