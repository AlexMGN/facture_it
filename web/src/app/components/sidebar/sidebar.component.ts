import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Tableau de bord',  icon: 'objects_spaceship', class: 'top' },
    { path: '/user-profile', title: 'Mon profil',  icon: 'users_single-02', class: '' },
    { path: '/facture-it', title: 'Se déconnecter',  icon: 'objects_planet', class: 'active active-pro bottom' }

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public isAuthenticated: any;

  constructor() { }

  menuItems = [
    { path: 'facture-it', title: 'Se déconnecter',  icon: 'fa-arrow-alt-circle-left', class: 'active active-pro bottom' }
  ];

  ngOnInit() {
    this.checkIsAuthenticated();
  }

  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  }

  checkIsAuthenticated() {
    const auth = localStorage.getItem('isAuthenticated');

    (!auth || auth === 'false') ? this.isAuthenticated = false : this.isAuthenticated = true;
  }
}
