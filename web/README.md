# [Facture-IT](https://bitbucket.org/AlexMGN/facture_it/src/develop/) 
[![version][version-badge]][CHANGELOG]

## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
5. Then: ```npm install```
6. And: ```npm start```
7. Navigate to [localhost:4200](localhost:4200)

[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE
[version-badge]: https://img.shields.io/badge/version-1.0.1-blue.svg
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
