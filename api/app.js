require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const cors = require('cors');
const { check } = require('express-validator');
const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');

const whitelist = [process.env.LOCAL_URL, process.env.PROD_URL]

const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors(corsOptions));

const restrictedRoutesMethods = require('./src/routes/restricted/restrictedRoutesMethods');
const restrictedRoutes = require('./src/routes/restricted/restrictedRoutes')(express.Router(), check, restrictedRoutesMethods);
const openRoutesMethods = require('./src/routes/open/openRoutesMethods');
const openRoutes = require('./src/routes/open/openRoutes')(express.Router(), check, openRoutesMethods);

app.use('/api', openRoutes);
app.use('/api/auth', restrictedRoutes);

app.use(express.static(__dirname + '/web/dist'));

app.get('/*', function(req,res) {
    
    try {
        if (fs.existsSync(path.join(__dirname+'/web/dist/index.html'))) {
            res.sendFile(path.join(__dirname+'/web/dist/index.html'));
        } else {
            res.sendFile(path.join(__dirname+'/index.html'));
        }
    } catch(err) {
        res.send(err)
    }
});

mongoose.connect(process.env.FACTURE_IT_CONNECT_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then((database) => {
    console.log('Connected to MongoDB !');
    // global.db = database
});

app.listen(port, () => {
    console.log('🚀 App listening on port: ' + port);
});
