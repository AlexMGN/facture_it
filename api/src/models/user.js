const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const UserSchema = new Schema({
    num: String,
    email: String,
    nom_societe: String,
    password: String,
    adresse: String,
    ville: String,
    cp: Number,
    pays: String,
    tel_port: String,
    tel_fix: String,
    access_token: String,
    forgot_token: String,
    trial: Boolean,
    bic: String,
    iban: String,
    tva: String,
    ape: String,
    siret: String,
    bank_action: String,
    bankin_uid: String,
    bankin_api_key: String,
    bank_connected_logo: String,
    bank_access_token: String,
    bank_balance: String,
    bank_connected_item: Boolean,
    bank_name: String,
    Clients: [{
        id: String,
        status: String,
        name: String,
        email: String,
        adresse: String,
        ville: String,
        cp: String,
        pays: String,
        tel_port: String,
        tel_fix: String,
        Factures: [{
            id: String,
            created_at: Date,
            limit_at: Date,
            num_facture: String,
            status: String,
            Items: [{
                name: String,
                description: String,
                quantite: Number,
                prix_unitaire: Number,
                total: Number
            }]
        }],
        Devis: [{
            id: String,
            created_at: Date,
            limit_at: Date,
            num_devis: String,
            status: String,
            Items: [{
                name: String,
                description: String,
                quantite: Number,
                prix_unitaire: Number,
                total: Number
            }]
        }]
    }]
});

UserSchema.pre('save', next => {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    next();
});

module.exports = mongoose.model('user', UserSchema);
