const { validateRequest } = require('../../utils/validators');

module.exports = (router, check, openRoutesMethods) => {

    /* Users */
    router.post('/user/register', validateRequest('register'), openRoutesMethods.register);
    router.post('/user/connect', validateRequest('connect'), openRoutesMethods.connect);
    router.post('/user/googleConnect', openRoutesMethods.googleConnect);

    //router.post('/connect', validateRequest('connect'), openRoutesMethods.register);
    /* Google */
    return router 

}
