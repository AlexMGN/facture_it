const {connectUserWithGoogle} = require("../../controllers/users");
const { validationResult } = require('express-validator');

const { registerNewUser, connectUser, generateId } = require('../../controllers/users');


const register = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).send({
            message: 'Erreur de validation : ',
            error: errors.array({ onlyFirstError: true })
        });
    } else {
        try {
            let data = {
                num: generateId('user'), 
                nom_societe: req.body.nom_societe,
                email: req.body.email.toLowerCase(),
                password: req.body.password,
                adresse: req.body.adresse,
                ville: req.body.ville,
                cp: req.body.cp,
                pays: req.body.pays,
                portable: req.body.portable,
                fixe: req.body.fixe,
                bic : req.body.fixe,
                iban : req.body.iban,
                tva : req.body.tva,
                ape : req.body.ape,
                siret : req.body.siret,
            };

            let new_user = await registerNewUser(data);

            res.status(200).send({
                message: new_user.message
            });
        } catch (err) {
            res.status(400).send({
                message: err
            });
        }
    }
};


const connect = async (req, res) => {
    
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).send({
            message: 'Erreur de validation : ',
            error: errors.array({ onlyFirstError: true })
        });
    } else {
        let data = {
            email: req.body.email,
            password: req.body.password
        }
        try {
            const token = await connectUser(data)
            
            res.status(200).send({
                message: "Vous êtes connecté(e) !",
                token
            })
        } catch(error) {
            res.status(400).send({
                message: "Echec lors de la connexion !"
            })
        }
    }
};


const googleConnect = async (req, res) => {
    try {
        const token = await connectUserWithGoogle(req.body.email, req.body.id)

        res.status(200).send({
            message: "Vous êtes connecté(e) !",
            token
        })
    } catch(error) {
        res.status(400).send({
            message: "Echec lors de la connexion !"
        })
    }
};

module.exports = {
    register, connect, googleConnect
};
