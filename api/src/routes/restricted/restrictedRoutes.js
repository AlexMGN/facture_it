const { validateRequest } = require('../../utils/validators');


module.exports = (router, check, restrictedRoutesMethods) => {

    /* Users */
    //router.post('/user',/* validateRequest('document'),*/ restrictedRoutesMethods.addUser);
    router.delete('/user',/* validateRequest('delete')*/ restrictedRoutesMethods.deleteUser);
    router.put('/user', /*validateRequest('update'),*/ restrictedRoutesMethods.updateUser)
    router.get('/user',/* validateRequest('document'),*/ restrictedRoutesMethods.getUser);
    //router.get('/bank/sold', restrictedRoutesMethods.getUserSoldeN26);

    /* Clients */
    router.post('/clients',/* validateRequest('document'),*/ restrictedRoutesMethods.addClient);
    router.post('/client',/* validateRequest('document'),*/ restrictedRoutesMethods.addClient);
    //router.delete('/client',/* validateRequest('document'),*/ restrictedRoutesMethods.deleteUser);
    //router.update('/client',/* validateRequest('document'),*/ restrictedRoutesMethods.updateUser);
    router.get('/clients',/* validateRequest('document'),*/ restrictedRoutesMethods.getClients);
    router.get('/client',/* validateRequest('document'),*/ restrictedRoutesMethods.getClientById);
    /* Factures */
    router.post('/factures',/* validateRequest('document'),*/ restrictedRoutesMethods.generateFacture);
    router.post('/facturespdf',/* validateRequest('document'),*/ restrictedRoutesMethods.generateFacturePdf);
    router.post('/facturesmail',/* validateRequest('document'),*/ restrictedRoutesMethods.sendMailFacture);
    /* Devis */
    router.post('/devis',/* validateRequest('document'),*/ restrictedRoutesMethods.generateDevis);
    router.post('/devispdf',/* validateRequest('document'),*/ restrictedRoutesMethods.generateDevisPdf);
    router.post('/devismail',/* validateRequest('document'),*/ restrictedRoutesMethods.sendMailDevis);
    /* Outils comptable */

    /* Statistiques */

    /*test */
    
    /* Banks */
    router.post('/openbanking/create/account', restrictedRoutesMethods.createAccountOnOpenBankingService)
    router.get('/openbanking/transactions', restrictedRoutesMethods.getTransactionsForUser)
    router.get('/openbanking/disconnect', restrictedRoutesMethods.disconnectBankinForUser)

    return router
}
