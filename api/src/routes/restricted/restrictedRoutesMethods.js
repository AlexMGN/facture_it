const { validationResult } = require('express-validator');
const { DateTime } = require("luxon");
const userInMongo = require("../../models/user")
const html_to_pdf = require('html-pdf-node');
const fs = require('fs');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const {disconnectUserForBankin} = require("../../controllers/banks");
const {getTransactions} = require("../../controllers/banks");
const {createBankinAccount} = require("../../controllers/banks");
const { updateUserInMongo, deleteUserInMongo, generateId, getUserById } = require('../../controllers/users');
const { getUserIdFromToken } = require('../../controllers/token')
const { addClientForAnUser, getAllClient, getOneClientById } = require('../../controllers/clients')
const { countFacture, saveFactureInMongo, generatePdfFromFacture, getFactureById, sendMailFactureTo   } = require('../../controllers/factures')
const { countDevis, saveDevisInMongo, generatePdfFromDevis, getDevisById, sendMailDevisTo   } = require('../../controllers/devis')
const { checkIfTokenIsValid } = require('../../utils/utils')
const {getN26Solde} = require("../../controllers/banks");


const getArticle = (client, nature) => {
    
    var document;
    if (devisOrFacture(nature) == "facture") {
      document = client.Clients[0].Factures.Items;
    }
    else { 
     document = client.Clients[0].Devis.Items;
    } 
    let bill = []

    for(let i = 0; i < document.length; i++) {
      bill.push(document[i]) 
    }
    return bill
}


const numberWithSpaces = (x)  => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}

const generateItem = (items) => {
    
    let listitem = ``;
    
    for(let i = 0; i < items.length;i++) {
      
      listitem += `
      <tr>
        <td class="service" style="text-align: left; vertical-align: top;
        border-top: 1px solid #5D6975;">${items[i].name}</td>
        <td class="desc" style="text-align: left; vertical-align: top;
        border-top: 1px solid #5D6975;">${items[i].description}</td>
        <td class="unit" style="font-size: 1.2em;
        border-top: 1px solid #5D6975; white-space: nowrap;">${numberWithSpaces(items[i].prix_unitaire)}€</td>
        <td class="qty" style="font-size: 1.2em;
        border-top: 1px solid #5D6975;">${items[i].quantite}</td>
        <td class="total" style="font-size: 1.2em;
        border-top: 1px solid #5D6975; white-space: nowrap;">${numberWithSpaces(items[i].prix_unitaire*items[i].quantite)}€</td>
      </tr>
      `
    }
    
    return listitem
}

const devisOrFacture = (nature) => {
    if(nature == "Devis")
      return "D-"
    return "F-"
}


const padNumber = (number) => {
    console.log("feu")
    number = number.toString();

    while(number.length < 5) {
        number = "0" + number;
    }
    console.log(number)
    return number;
}

const generateTotal = (items) => {
    
    let total = 0;
    for(let i = 0; i < items.length;i++) {
      total = total + parseFloat(parseFloat(items[i].prix_unitaire*parseFloat(items[i].quantite)));
    }
    total = total;
    return numberWithSpaces(total)
  
}

//enlever email <div><span>EMAIL</span> ${req.body.clientBill.client.email}</div>
//génère la partie client en haut à gauche du pdf ${dt.toFormat('dd MMM yyyy')}${dueDate.toFormat('dd MMM yyyy')}
const generateClient = (name, adresse, code_postal) => {
    let dt = DateTime.local().setLocale("fr");
    let dueDate = dt.plus( {months: 3}).setLocale("fr");
      let listitem = `
        <div style="widht = 100%">
          <div><span>CLIENT</span>${name}</div>
          <div><span>ADDRESS</span>${adresse}, ${code_postal}</div>
          <div><span>DATE</span>${dt.toFormat('dd MMM yyyy')}</div>
          <div><span>DUE DATE</span>${dueDate.toFormat('dd MMM yyyy')}</>
          </div>
        </div>
      `
      return listitem;
}


const updateUser = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
      let data = {
        email: req.body.email.toLowerCase(),
        nom_societe: req.body.nom_societe,
        bic : req.body.bic,
        iban : req.body.iban,
        tva : req.body.tva,
        ape : req.body.ape,
        siret : req.body.siret,
        adresse: req.body.adresse,
        ville: req.body.ville,
        cp: req.body.cp,
        pays: req.body.pays,
        tel_port: req.body.portable,
        tel_fix: req.body.fixe,
    };
    
    
      try {
        const TokenArray = req.headers['authorization'].split(' ');
        const token = await checkIfTokenIsValid(TokenArray[1])
        const userId = token.id
        
        
        await updateUserInMongo(data, userId)
        .then(() => {
          res.status(200).send({
              message: "updateUser : Les données de l'utilisateur on été mis à jour !"
          })
        }) 
      }
      catch {
        res.status(400).send({   
          message: "updateUser : Echec lors de la fonction !"
      })
      }
  }         
}



const getUser = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      const token = req.headers.authorization;
      const userInfo = await checkIfTokenIsValid(token)

      const user = await getUserById(userInfo.id)

      res.status(200).send({
        message: "getUser : Les données de l'utilisateur on été correctement récupéré !",
        user
      })
    } catch(err) {
      res.status(400).send({   
        message: "getUser : Echec de la fonction"
      })
    }
  }
  
}


const deleteUser = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      const user = await deleteUserInMongo(userId)
      .then(() => {
        res.status(200).send({   
          message: "deleteUser : L'utilisateur à été supprimé avec succès !"
        })
      })
    } catch(err) {
      res.status(400).send({   
        message: "deleteUser : Echec lors l'execution de la fonction !"
      })
    }
  }
}


const addClient = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      //console.log("entrée : try addClient")
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      
      const data = {
        id: generateId('client'),
        name:req.body.name,
        email:req.body.email,
        adresse:req.body.adresse,
        ville:req.body.ville,
        cp:req.body.cp,
        pays:req.body.pays,
        tel_port:req.body.tel_port,
        tel_fix:req.body.tel_fix,
        Facture: [],
        Devis: []
      }
      await addClientForAnUser(userId, data)
      //console.log("doubi")

      res.status(200).send({
        message: 'Success'
      });
    }
    catch (error) {
      res.status(400).send({
        message: 'Error'
      });
    }
  }
}

const generateFacture = async (req, res) => {
  
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      const user = await getUserById(userId)
      
      const clientId = 'CLItNr13RTdVi12';//l'id sera transmis sur la page lorsque l'utilisateur séléctionne
      
      const client = await getOneClientById(userId, clientId)


      const data = {
        // id: generateId('facture'),
        // created_at: DateTime.local().setLocale("fr"),
        // limit_at: DateTime.local().setLocale("fr").plus( {months: 3}),
        // num_facture: 'String',//appel fonction count
        // status: 'impayée',
        // Items: req.body.Items
      }
      
      await saveFactureInMongo(user, client, req.body.Items)
    
      res.status(200).send({
        message: 'Success'
      });
    }
    catch (error) {
      res.status(400).send({
        message: 'Error'
      });
    }
  }
}

const generateDevis = async (req, res) => {
  
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      const user = await getUserById(userId)
      
      const clientId = 'CLItNr13RTdVi12';//l'id sera transmis sur la page lorsque l'utilisateur séléctionne
      
      const client = await getOneClientById(userId, clientId)

      
      await saveDevisInMongo(user, client, req.body.Items)
    
      res.status(200).send({
        message: 'Success'
      });
    }
    catch (error) {
      res.status(400).send({
        message: 'Error'
      });
    }
  }
}

const getClients = async (req,res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      const clients = await getAllClient(userId);
      res.status(200).send({
        message: "getClients : Les clients de l'utilisateur on été correctement récupéré !",
        clients
      })
    } catch(err) {
      res.status(400).send({   
        message: "getClients: Echec de la fonction"
      })
    }
  }
}

const getClientById = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      const clientId = 'CLI4PL8Hb4grAu7';//req.body
      const client = await getOneClientById(userId, clientId);
      res.status(200).send({
        message: "getClients : Le client de l'utilisateur a été correctement récupéré !",
        client
      })
    } catch(err) {
      res.status(400).send({   
        message: "getClients: Echec de la fonction"
      })
    }
  }
}

const getClientByName = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({
      message: 'Erreur de validation : ',
      error: errors.array({ onlyFirstError: true })
    });
  } else {
    try {
      const TokenArray = req.headers['authorization'].split(' ');
      const token = await checkIfTokenIsValid(TokenArray[1])
      const userId = token.id
      const clientName = 'Total';//req.body
      const client = await getOneClientByName(userId, clientName);
      res.status(200).send({
        message: "getClients : Le client de l'utilisateur a été correctement récupéré !",
        client
      })
    } catch(err) {
      res.status(400).send({   
        message: "getClients: Echec de la fonction"
      })
    }
  }
}

const generateFacturePdf = async (req, res) => {
  try{
    
    const TokenArray = req.headers['authorization'].split(' ');
    const token = await checkIfTokenIsValid(TokenArray[1])
    const userId = token.id
    const user = await getUserById(userId)
    
    const clientId = 'CLItNr13RTdVi12';//l'id sera transmis sur la page lorsque l'utilisateur séléctionne
      
    const client = await getOneClientById(userId, clientId)
    
    const factureId = 'FACilSDCenk78rK';
    const facture = await getFactureById(user._id, clientId, factureId);
    
    await generatePdfFromFacture(user, client, facture);
    
    
    res.status(200).send({   
      message: "generatePdfFacture: Succes de la fonction"
      })
  }
  catch(err) {
    res.status(400).send({   
    message: "generatePdfFacture: Echec de la fonction"
    })
  }
}

const generateDevisPdf = async (req, res) => {
  try{
    
    const TokenArray = req.headers['authorization'].split(' ');
    const token = await checkIfTokenIsValid(TokenArray[1])
    const userId = token.id
    const user = await getUserById(userId)
    
    const clientId = 'CLItNr13RTdVi12';//l'id sera transmis sur la page lorsque l'utilisateur séléctionne
      
    const client = await getOneClientById(userId, clientId)
    
    const devisId = '';//mettre id devis
    const devis = await getDevisById(user._id, clientId, devisId);
    
    await generatePdfFromDevis(user, client, devis);
    
    
    res.status(200).send({   
      message: "generatePdfDevis: Succes de la fonction"
      })
  }
  catch(err) {
    res.status(400).send({   
    message: "generatePdfDevis: Echec de la fonction"
    })
  }
}

const sendMailFacture = async (req, res) => {
  try{
    const TokenArray = req.headers['authorization'].split(' ');
    const token = await checkIfTokenIsValid(TokenArray[1])
    const userId = token.id
    const user = await getUserById(userId)
    await sendMailFactureTo(user);
    

    res.status(200).send({   
      message: "sendMail: Succes de la fonction"
      })
  }
  catch(err) {
    res.status(400).send({   
      message: "sendMail: Echec de la fonction"
      })
  }
}

const sendMailDevis = async (req, res) => {
  try{
    const TokenArray = req.headers['authorization'].split(' ');
    const token = await checkIfTokenIsValid(TokenArray[1])
    const userId = token.id
    const user = await getUserById(userId)
    await sendMailDevisTo(user);
    res.status(200).send({   
      message: "sendMailDevis: Succes de la fonction"
      })
  }
  catch(err) {
    res.status(400).send({   
      message: "sendMailDevis: Echec de la fonction"
      })
  }
}

const createAccountOnOpenBankingService = async (req, res) => {
  const token = req.headers.authorization;

  if (!token) {
      res.status(400).send({
          message: 'No authorization'
      });
  } else {
      try {
        const verified = await checkIfTokenIsValid(token);

        const userBanksURL = await createBankinAccount(verified.id);

        res.status(200).send({
          success: true,
          userBanksURL
        });
      } catch (e) {
          res.status(400).send({
              message: 'No authorization',
              err: e.message
          });
      }
  }
};

const getTransactionsForUser = async (req, res) => {
  const token = req.headers.authorization;

  if (!token) {
      res.status(400).send({
          message: 'No authorization'
      });
  } else {
      try {
        const verified = await checkIfTokenIsValid(token);

        const transactions = await getTransactions(verified.id);

        res.status(200).send({
          success: true,
          transactions
        });
      } catch (e) {
          res.status(400).send({
              message: 'No authorization',
              err: e.message
          });
      }
  }
};

const disconnectBankinForUser = async (req, res) => {
  const token = req.headers.authorization;

  if (!token) {
      res.status(400).send({
          message: 'No authorization'
      });
  } else {
      try {
        const verified = await checkIfTokenIsValid(token);

        const disconnected = await disconnectUserForBankin(verified.id);

        res.status(200).send({
          success: true,
          disconnected
        });
      } catch (e) {
          res.status(400).send({
              message: 'No authorization',
              err: e.message
          });
      }
  }
};



module.exports = {
    updateUser, getUser, deleteUser, addClient, generateFacture, getArticle, getClients, getClientById, generateClient, generateFacturePdf, generateItem,
    generateClient, numberWithSpaces, generateTotal, padNumber, sendMailFacture, createAccountOnOpenBankingService, generateDevis, generateDevisPdf, sendMailDevis,
  getTransactionsForUser, disconnectBankinForUser
};


