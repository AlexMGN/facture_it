const jwt = require('jsonwebtoken');

const generateAccessToken = async (user) => {
    if (user.bank_access_token) {
        return jwt.sign({ id: user._id, bankin_token: user.bank_access_token }, process.env.JWT_ENCRYPTION);
    }

    return await jwt.sign({ id: user._id }, process.env.JWT_ENCRYPTION);
}

    
module.exports = {
    generateAccessToken
};
