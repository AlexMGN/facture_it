require('dotenv').config()
const User = require('../models/user');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');


const { generateAccessToken } = require('./token');



const registerNewUser = async (user) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const userExist = await User.findOne({ email: user.email });
        if (userExist.length > 0) {
            return {
                message: "Il semblerait que vous existez déjà chez nous"
            }
        } else {
            
            
            const hashedPassword = await bcrypt.hash(user.password, salt);
    
            let user_credentials = {
                email: user.email,
                num: user.num,
                nom_societe: user.nom_societe,
                password: hashedPassword,
                adresse: user.adresse,
                ville: user.ville,
                cp: user.cp,
                pays: user.pays,
                tel_port: user.portable,
                tel_fixe: user.fixe,
                trial: true,
                isActive: true,
                bic : user.fixe,
                iban : user.iban,
                tva : user.tva,
                ape : user.ape,
                siret : user.siret,
                createdAt: new Date()
            };
    
            User(user_credentials).save((e) => {
                if (e) {
                    throw new Error('Erreur lors de l\'inscription');
                }
            });
    
            return {
                message: "Inscription réussie !"
            }
        }
    }
    catch (err) {

        throw new Error('registerNewUser : Erreur lors de l\'inscription');
    }
};

const getUserById = async (userId) => {
    try {
        const user = await User.findById(userId);
        if (!user) {
            throw new Error("Aucun utilisateur trouvé")      
        }      
        return user
    }
    catch(err) {
        throw new Error("Erreur lors de la recherche de l'utilisateur par id");
    }
};
//a supp peut etre
const checkPassword = async (email, password) => { 
    try {
        const user = await getUserByEmail(email);
        const res = await bcrypt.compare(password, user.password)
        return res
    }
    catch(err) {
        throw new Error(err)
    }  
}

const getUserByEmail = async (userEmail) => {
    try {
        const userEmail_mod = userEmail.toLowerCase()
        const user = await User.findOne({email: userEmail_mod})
        if (!user) {
            throw new Error("Aucun utilisateur trouvé")
        }
        return user
    }
    catch(err) {
        throw new Error("getUserByEmail : Erreur lors de la recherche de l'utilisateur")
    }

}

const connectUser = async (data) => {
    
    try {
        const user_credentials = {
            email: data.email,
            password: data.password
        }
        const user = await getUserByEmail(user_credentials.email)
        const isPassCorrect =  bcrypt.compare(user_credentials.password, user.password)
       
        if(!isPassCorrect){
            console.log("connectUser : Impossible de se connecter\n")
            console.log("le token n'est pas généré\n")
            throw new Error('connectUser : Impossible de se connecter\n')  
        } else {
            const  token = generateAccessToken(user)
            console.log("Le token suivant à été généré : " + token)
            return token
        }
    }
    catch(err) {
        throw new Error('connectUser : Impossible de récupérer les données utilisateurs avec le mail !')
    }
}

const connectUserWithGoogle = async (email, password) => {
    try {
        const user = await User.findOne({ email })

        if (!user) {
            console.log(email)
            console.log(password)
            User({ email, password }).save(async (e, res) => {
                if (e) {
                    throw new Error('Erreur lors de l\'inscription');
                }

                return await generateAccessToken(res)
            });
        } else {
            return await generateAccessToken(user)
        }
    }
    catch(err) {
        throw new Error('connectUser : Impossible de récupérer les données utilisateurs avec le mail !')
    }
}

const updateUserInMongo = async (data, userId) => {
    const errors = validationRequest("updateUserInMongo");
  
    if (!errors.isEmpty()) {
    
      res.status(400).send({
          message: 'Erreur de validation : ',
          error: errors.array({ onlyFirstError: true })
      });
    }
    try {
        // const data_fill = await fillNullDataUser(data, userId)
        await User.updateOne({_id: userId},
            { $set: { 
                email: data.email,
                nom_societe: data.nom_societe,
                adresse: data.adresse,
                ville: data.ville,
                cp: data.cp,
                pays: data.pays,
                tel_port: data.tel_port,
                tel_fixe: data.tel_fixe,
                bic : data.bic,
                iban : data.iban,
                tva : data.tva,
                ape : data.ape,
                siret : data.siret,
              } 
            } 
        )
        console.log("Updated Users : ", users);
    }
    catch(err) {
        throw new Error('updateUserInMongo : Erreur lors de la mise à jour de la base de données')
    }
}


function rand(length, ...ranges) {
    var str = "";                                                       // the string (initialized to "")
    while(length--) {                                                   // repeat this length of times
      var ind = Math.floor(Math.random() * ranges.length);              // get a random range from the ranges object
      var min = ranges[ind][0].charCodeAt(0),                           // get the minimum char code allowed for this range
          max = ranges[ind][1].charCodeAt(0);                           // get the maximum char code allowed for this range
      var c = Math.floor(Math.random() * (max - min + 1)) + min;        // get a random char code between min and max
      str += String.fromCharCode(c);                                    // convert it back into a character and append it to the string str
    }
    return str;                                                         // return str
}

const generateId = (type) => {
    switch (type) {
        case 'user':
          console.log('génère un ID pour un utilisateur');
          return 'USE'+rand(12, ["A", "Z"], ["0", "9"], ["a", "z"])
        case 'client':
            console.log('génère un ID pour un client');
            return 'CLI'+rand(12, ["A", "Z"], ["0", "9"], ["a", "z"])
        case 'devis':
            console.log('génère un ID pour un devis');
            return 'DEV'+rand(12, ["A", "Z"], ["0", "9"], ["a", "z"])
        case 'facture':
            console.log('génère un ID pour un facture');
            return 'FAC'+rand(12, ["A", "Z"], ["0", "9"], ["a", "z"])
        default:
          console.log(`Le paramètre saisie est incorrect`);
          throw new Error(`Le paramètre saisie est incorrect`)
      }
}

const deleteUserInMongo = async (userId) => {
    try {
        await User.DeleteOne({_id: userId})
        console.log("Deleted User : ", users);
        return true
    }
    catch(err) {
        throw new Error("deleteUserInMongo : Erreur lors de la suppression dans la base de données")
    }
}

module.exports = {
    registerNewUser, connectUser, updateUserInMongo, generateId, deleteUserInMongo, getUserById, connectUserWithGoogle
};
