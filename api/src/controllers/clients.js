const User = require('../models/user');
const mongoose = require('mongoose');


const addClientForAnUser = async (userId, data) => {
    
    try {
        await User.updateOne({_id: userId},
        { $push: { 
            Clients: [{
                id: data.id,
                name:data.name,
                email:data.email,
                adresse:data.adresse,
                ville:data.ville,
                cp:data.cp,
                pays:data.pays,
                tel_port:data.tel_port,
                tel_fix:data.tel_fix,
                Facture: [],
                Devis: []
            }]
          } 
        })
        return true
    } 
    catch (e) {
        console.log(e)
        throw new Error("addClientForAnUser : Erreur lors de l'ajout du client dans la base de données")
    }
}

const getAllClient = async (userId) => {
    try {
        const user = await User.findById({_id: userId});
        if (user.Clients.length <= 0) {
            throw new Error("Aucun client trouvé")
        }
        return user.Clients
    }
    catch(err) {
        throw new Error("getAllClient : Erreur lors de la recherche de l'utilisateur")
    }
}

const getOneClientByName = async (userId, clientName) => {
    try {
        const user = await User.findById({_id: userId});
        if (user.Clients.length <= 0) {
            throw new Error("Aucun client trouvé")
        }
        
        for (let i = 0; i < user.Clients.length-1; i++) {
            if (user.Clients[i].name !== clientName) {
                return user.Clients[i]
            } 
            else {
                throw new Error("getOneClientByName : Impossible de trouver le client")
            }
            
        }
    }
    catch(err) {
        throw new Error("getAllClient : Erreur lors de la recherche de l'utilisateur")
    }
}

const getOneClientById = async (userId, clientId) => {
    
    try {
        const user = await User.findById({_id: userId});
        
        if (user.Clients.length <= 0) {
            throw new Error("Aucun client trouvé")
        }

        for (let i = 0; i < user.Clients.length; i++) {
            if (user.Clients[i].id === clientId) {
                return user.Clients[i]
            }  
        }
        throw new Error("getOneClientById : Impossible de trouver le client")
    }
    catch(err) {
        throw new Error("getAllClient : Erreur lors de la recherche de l'utilisateur")
    }
}

const getIndiceClient= async (userId, clientId) => {
    try {
        const user = await User.findById({_id: userId});
        if (user.Clients.length <= 0) {
            throw new Error("Aucun client trouvé")
        }
        for (let i = 0; i < user.Clients.length-1; i++) {
            if (user.Clients[i].id === clientId) {
                return i
            } else {
                throw new Error("getIndiceClient : Impossible de trouver l'indice du client")
            }   
        }
    }
    catch(err) {
        throw new Error("getIndiceClient : Impossible de trouver l'indice du client")
    }
}
module.exports = {
    addClientForAnUser, getAllClient, getOneClientById, getIndiceClient
};