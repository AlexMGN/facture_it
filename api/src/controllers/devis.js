require('dotenv').config()
const User = require('../models/user');
const mongoose = require('mongoose');
const { DateTime } = require("luxon");
const { updateUserInMongo, deleteUserInMongo, generateId } = require('../controllers/users')
const { addClientForAnUser, getAllClient, getOneClientById } = require('../controllers/clients')
const html_to_pdf = require('html-pdf-node');
const fs = require('fs');
const nodemailer = require('nodemailer');
const { generateHtmlDevis } = require('../controllers/pdf')


const countDevis = async (userId, clientId) => {

    const countQuery = await getOneClientById(userId, clientId)
 
    return countQuery.Devis.length
}


const saveDevisInMongo = async (user, client, bill) => {
    try {     
        let number = await countDevis(user._id, client.id) + 1;

        data = {
                id: generateId("devis"),
                created_at: DateTime.local().setLocale("fr"),
                limit_at: DateTime.local().setLocale("fr").plus( {months: 3}),
                num_devis: number,
                status: "En attente de validation",
                Items: bill
              }
        
        
        await User.updateOne({
            _id: user._id ,
            "Clients.id": client.id},
          {
            "$push": {
                "Clients.$.Devis": data,
            }
          })
    }
    catch(err) {
        throw new Error("saveDevisInMongo: Error")
    }
}


const sendMailDevisTo = async (user) => {
    try {
    console.log(process.env.MAIL_ID)
    console.log(process.env.MAIL_PASSWORD)
    const transporter = nodemailer.createTransport({
        port: 465,               // true for 465, false for other ports
        host: "smtp.gmail.com",
           auth: {
                user: process.env.MAIL_ID,
                pass: process.env.MAIL_PASSWORD,
             },
        secure: true,
    });
  
    const mailData =  {
        from: process.env.MAIL_ID, 
        to: "benjamin.hagege6@gmail.com", //la je dois coder une fonction qui recupere le mail depuis le token
        subject: "Prestation de service - "+user.name, // la meme ici pour le username
        text: "", // plain text body
        html: "Vous trouverez ci-joint la devis, correspondant à la mission que vous m’avez confié et je vous en remercie. <br> Bien à vous, "+user.name, // html body
        attachments: [{
          filename:'devis.pdf', 
                  contentType: 'application/pdf',
          path: './src/tmp/devis.pdf'
      }]
    };
  
    transporter.sendMail(mailData, function (err, res) {
        if(err)
            console.log(err)
        else
            console.log("sent");          
    });
    } catch(err) {
        throw new Error("impossible d'envoyer un mail")
    }
}




const generatePdfFromDevis = async (user, client, devis) => {
    try {
    const html = generateHtmlDevis(user,client, devis);

    const options = { format: 'A4' };
    const file = { content: html };
    
    
    await html_to_pdf.generatePdf(file, options).then(pdfBuffer => {
        fs.writeFileSync('./src/tmp/devis.pdf', pdfBuffer)
    });
    
    } catch(err) {
        throw new Error('GeneratePdf')
    }
    
}

const getDevisById = async (userId, clientId, devisId) => {
    try {

        const user = await User.findOne({
            _id: userId,
        })
        var i = 0;
        while(user.Clients[i].id !== clientId && i < user.Clients.length) {
            i++;
        }
        var x = 0;
        while(user.Clients[i].Devis[x].id !== devisId && x < user.Clients[i].Devis.length) {
            x++;
        }
        return user.Clients[i].Devis[x]
    }catch(err) {
        throw new Error("saveDevisInMongo: Error")
    }
}
module.exports = {
    countDevis, saveDevisInMongo, generatePdfFromDevis, getDevisById, sendMailDevisTo
};