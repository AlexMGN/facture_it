const User = require('../models/user');
const axios = require('axios');

const createBankinAccount = async (userId) => {
    const user = await User.findById(userId);

    if (!user.bankin_uid) {
        try {
            const bankinAPIKeyForUser = 'Bankin_API_' + user.password.slice(8);
            await User.updateOne({ _id: userId }, {
                bankin_api_key: bankinAPIKeyForUser
            })

            await axios.post('https://sync.bankin.com/v2/users', {
                email: user.email,
                password: bankinAPIKeyForUser
            }, {
                headers: {
                    "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                    'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                    'Bankin-Version': '2019-02-18'
                }
            })

            const connectUserToBankin = await axios.post('https://sync.bankin.com/v2/authenticate', {
                email: user.email,
                password: bankinAPIKeyForUser
            }, {
                headers: {
                    "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                    'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                    'Bankin-Version': '2019-02-18'
                }
            })

            await User.updateOne({ _id: userId }, {
                bankin_uid: connectUserToBankin.data.user.uuid,
                bank_access_token: connectUserToBankin.data.access_token
            })

            const connectBanksURL = await connectToUserBankAccount(connectUserToBankin.data.access_token);

            await User.updateOne({ _id: userId }, {
                bank_connected_item: true,
                bank_action: 'connected'
            })

            return connectBanksURL.data.redirect_url
        } catch (e) {
            console.log(e)
            throw new Error(e.response.data.message)
        }
    } else {
        try {
            const connectBanksURL = await connectToUserBankAccount(user.bank_access_token);

            await User.updateOne({ _id: userId }, {
                bank_connected_item: true,
                bank_action: 'connected'
            })

            return connectBanksURL.data.redirect_url
        } catch (e) {
            console.log(e.response.data)
            if (e.response.data.type === "expired_token" || e.response.data.type === "invalid_token") {
                const refreshedToken = await authenticatedUser(user.email, user.bankin_api_key);

                await User.updateOne({ _id: user._id }, {
                    bank_access_token: refreshedToken.data.access_token
                })

                await createBankinAccount(user._id);
            }
            throw new Error(e.response.data.message)
        }
    }
}

const connectToUserBankAccount = async (user_bankin_token) => {
    return await axios.get('https://sync.bankin.com/v2/connect/items/add/url', {
        headers: {
            "Client-Id": '7a96abc258c540898fd6a872e06421b2',
            'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
            'Bankin-Version': '2019-02-18',
            'Authorization': 'Bearer ' + user_bankin_token
        }
    })
}

const getTransactions = async (userId) => {
    const user = await User.findById(userId);

    try {
        const items = await axios.get('https://sync.bankin.com/v2/items', {
            headers: {
                "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                'Bankin-Version': '2019-02-18',
                'Authorization': 'Bearer ' + user.bank_access_token
            }
        })

        if (items.data.resources[0].status_code_info === 'OK') {
            const account = await axios.get('https://sync.bankin.com/v2/accounts/' + items.data.resources[0].accounts[0].id, {
                headers: {
                    "Authorization": 'Bearer ' + user.bank_access_token,
                    "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                    'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                    'Bankin-Version': '2019-02-18',
                }
            })

            const bankLogo = await axios.get('https://sync.bankin.com/v2/banks/' + account.data.bank.id, {
                headers: {
                    "Authorization": 'Bearer ' + user.bank_access_token,
                    "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                    'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                    'Bankin-Version': '2019-02-18',
                }
            })

            await User.updateOne({ _id: user._id }, {
                bank_balance: account.data.balance,
                bank_name: bankLogo.data.name,
                bank_connected_logo: bankLogo.data.logo_url
            })

            const transactions = await axios.get('https://sync.bankin.com/v2/transactions', {
                headers: {
                    "Authorization": 'Bearer ' + user.bank_access_token,
                    "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                    'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                    'Bankin-Version': '2019-02-18',
                }
            })

            const allTransactions = [];

            for (let i = 0; i < transactions.data.resources.length; i++) {
                allTransactions.push({
                    status: defineStatus(transactions.data.resources[i].amount),
                    resource_type: defineTransactionType(transactions.data.resources[i].resource_type),
                    description: transactions.data.resources[i].description,
                    amount: JSON.stringify(transactions.data.resources[i].amount),
                    date: transactions.data.resources[i].date
                })
            }

            return allTransactions;
        } else {
            throw new Error("Aucune réponse de votre banque. Veuillez réessayer")
        }
    } catch (e) {
        if (e.response.data.type === "expired_token") {
            const refreshedToken = await authenticatedUser(user.email, user.bankin_api_key);

            await User.updateOne({ _id: user._id }, {
                bank_access_token: refreshedToken.data.access_token
            })

            await getTransactions(user._id);
        }
        throw new Error(e.response.data.message)
    }
}

const disconnectUserForBankin = async (userId) => {
    const user = await User.findById(userId);

    try {
        const disconnected = await axios.post('https://sync.bankin.com/v2/logout', {
            headers: {
                "Client-Id": '7a96abc258c540898fd6a872e06421b2',
                'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
                'Bankin-Version': '2019-02-18',
                'Authorization': 'Bearer ' + user.bank_access_token
            }
        })

        console.log(disconnected)
    } catch (e) {
        if (e.response.data.type === "expired_token") {
            const refreshedToken = await authenticatedUser(user.email, user.bankin_api_key);

            await User.updateOne({ _id: user._id }, {
                bank_access_token: refreshedToken.data.access_token
            })

            await disconnectUserForBankin(user._id);
        }

        console.log(e.response.data)
        //throw new Error(e.response.data.message)
    }
}

const defineStatus = (amount) => {
    return (Math.sign(amount) === 1) ? 'up' : 'down'
}

const defineTransactionType = (type) => {
    return (type === "transaction") ? 'virement' : ''
}

const authenticatedUser = async (email, bankinAPIKeyForUser) => {
    return await axios.post('https://sync.bankin.com/v2/authenticate', {
        email,
        password: bankinAPIKeyForUser
    }, {
        headers: {
            "Client-Id": '7a96abc258c540898fd6a872e06421b2',
            'Client-Secret': 'tQSf0bNkBeBsRS60OrPBGKi3H47RXArcA0GPlNh7v29x5L380i1hc1QWmZ9Vrvwb',
            'Bankin-Version': '2019-02-18'
        }
    })
}

module.exports = {
    createBankinAccount,
    getTransactions,
    disconnectUserForBankin
};
