var jwt = require('jsonwebtoken');



const checkIfTokenIsValid = async (token) => {
    try {
        return await jwt.verify(token, process.env.JWT_ENCRYPTION)
    } catch (e) {
        throw new Error('Token not valid');
    }
}


module.exports = {
    checkIfTokenIsValid
};