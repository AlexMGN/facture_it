import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateBillsPage } from './create-bills.page';

describe('CreateBillsPage', () => {
  let component: CreateBillsPage;
  let fixture: ComponentFixture<CreateBillsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBillsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateBillsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
