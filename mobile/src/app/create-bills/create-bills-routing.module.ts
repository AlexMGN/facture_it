import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateBillsPage } from './create-bills.page';

const routes: Routes = [
  {
    path: '',
    component: CreateBillsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateBillsPageRoutingModule {}
