import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateBillsPageRoutingModule } from './create-bills-routing.module';

import { CreateBillsPage } from './create-bills.page';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    CreateBillsPageRoutingModule
  ],
  declarations: [CreateBillsPage]
})
export class CreateBillsPageModule {}
