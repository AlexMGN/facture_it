import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-facture-tab',
  templateUrl: './facture-tab.page.html',
  styleUrls: ['./facture-tab.page.scss'],
})
export class FactureTabPage implements OnInit {

  client: any = [];

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.client = history.state.client;
    localStorage.setItem('client', JSON.stringify(this.client));
  }

  goBack() {
    if (history.state.service === 'professionnel') {
      this.router.navigate(['/me/home/professionnel']);
      localStorage.removeItem('client');
    } else {
      this.router.navigate(['/me/home/particulier']);
      localStorage.removeItem('client');
    }
  }

  goToProfil() {
    this.router.navigate(['/profil-page'], {
      state: {
        service: 'ficheFactures',
        client: history.state.client
      }
    });
  }

  createBill() {
    this.router.navigate(['/create-bills']);
  }

  createDevis() {
    this.router.navigate(['/create-devis']);
  }

}
