import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devis-tab',
  templateUrl: './devis-tab.page.html',
  styleUrls: ['./devis-tab.page.scss'],
})
export class DevisTabPage implements OnInit {

  client: any = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.client = JSON.parse(localStorage.getItem('client'));
  }

  goBack() {
    if (history.state.service === 'professionnel') {
      this.router.navigate(['/me/home/professionnel']);
      localStorage.removeItem('client');
    } else {
      this.router.navigate(['/me/home/particulier']);
      localStorage.removeItem('client');
    }
  }

  goToProfil() {
    this.router.navigate(['/profil-page'], {
      state: {
        service: 'ficheDevis',
        client: this.client
      }
    });
  }

  createBill() {
    this.router.navigate(['/create-bills']);
  }

  createDevis() {
    this.router.navigate(['/create-devis']);
  }

}
