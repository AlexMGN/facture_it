import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./landing-page/landing-page.module').then( m => m.LandingPagePageModule)
  },
  {
    path: 'me',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login-page',
    loadChildren: () => import('./login-page/login-page.module').then( m => m.LoginPagePageModule)
  },
  {
    path: 'register-page',
    loadChildren: () => import('./register-page/register-page.module').then( m => m.RegisterPagePageModule)
  },
  {
    path: 'forgot-page',
    loadChildren: () => import('./forgot-page/forgot-page.module').then( m => m.ForgotPagePageModule)
  },
  {
    path: 'profil-page',
    loadChildren: () => import('./profil-page/profil-page.module').then( m => m.ProfilPagePageModule)
  },
  {
    path: 'password-setting-page',
    loadChildren: () => import('./password-setting-page/password-setting-page.module').then( m => m.PasswordSettingPagePageModule)
  },
  {
    path: 'profil-setting-page',
    loadChildren: () => import('./profil-setting-page/profil-setting-page.module').then( m => m.ProfilSettingPagePageModule)
  },
  {
    path: 'add-client',
    loadChildren: () => import('./add-client/add-client.module').then( m => m.AddClientPageModule)
  },
  {
    path: 'fiche',
    loadChildren: () => import('./fiche-client-tab/fiche-client-tab.module').then( m => m.FicheClientTabPageModule)
  },
  {
    path: 'facture-tab',
    loadChildren: () => import('./facture-tab/facture-tab.module').then( m => m.FactureTabPageModule)
  },
  {
    path: 'devis-tab',
    loadChildren: () => import('./devis-tab/devis-tab.module').then( m => m.DevisTabPageModule)
  },
  {
    path: 'create-bills',
    loadChildren: () => import('./create-bills/create-bills.module').then( m => m.CreateBillsPageModule)
  },
  {
    path: 'bill-article',
    loadChildren: () => import('./bill-article/bill-article.module').then( m => m.BillArticlePageModule)
  },
  {
    path: 'bill-article-description',
    loadChildren: () => import('./bill-article-description/bill-article-description.module').then( m => m.BillArticleDescriptionPageModule)
  },
  {
    path: 'bill-article-price',
    loadChildren: () => import('./bill-article-price/bill-article-price.module').then( m => m.BillArticlePricePageModule)
  },
  {
    path: 'bill-recap',
    loadChildren: () => import('./bill-recap/bill-recap.module').then( m => m.BillRecapPageModule)
  },
  {
    path: 'create-devis',
    loadChildren: () => import('./create-devis/create-devis.module').then( m => m.CreateDevisPageModule)
  },
  {
    path: 'devis-article',
    loadChildren: () => import('./devis-article/devis-article.module').then( m => m.DevisArticlePageModule)
  },
  {
    path: 'devis-article-description',
    loadChildren: () => import('./devis-article-description/devis-article-description.module').then( m => m.DevisArticleDescriptionPageModule)
  },
  {
    path: 'devis-article-price',
    loadChildren: () => import('./devis-article-price/devis-article-price.module').then( m => m.DevisArticlePricePageModule)
  },
  {
    path: 'devis-recap',
    loadChildren: () => import('./devis-recap/devis-recap.module').then( m => m.DevisRecapPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
