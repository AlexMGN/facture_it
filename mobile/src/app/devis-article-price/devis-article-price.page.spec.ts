import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisArticlePricePage } from './devis-article-price.page';

describe('DevisArticlePricePage', () => {
  let component: DevisArticlePricePage;
  let fixture: ComponentFixture<DevisArticlePricePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisArticlePricePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisArticlePricePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
