import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisArticlePricePageRoutingModule } from './devis-article-price-routing.module';

import { DevisArticlePricePage } from './devis-article-price.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevisArticlePricePageRoutingModule
  ],
  declarations: [DevisArticlePricePage]
})
export class DevisArticlePricePageModule {}
