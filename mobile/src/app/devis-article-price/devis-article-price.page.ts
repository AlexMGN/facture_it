import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devis-article-price',
  templateUrl: './devis-article-price.page.html',
  styleUrls: ['./devis-article-price.page.scss'],
})
export class DevisArticlePricePage implements OnInit {

  client: any = [];
  counter: any;

  priceArticleCreateDevisCredentials = {
    quantity: '',
    price: ''
  };

  constructor(private router: Router) { }

  ngOnInit() {
    this.client = JSON.parse(localStorage.getItem('client'));
    this.counter = parseInt(localStorage.getItem('devis_counter'), 10);
  }

  goBack() {
    const articleDescription = localStorage.getItem('devis_name_article_' + this.counter);
    this.router.navigate(['/devis-article-description'], {
      state: {
        description: JSON.parse(articleDescription)
      }
    });
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateDevis() {
    localStorage.setItem('devis_description_price_' + this.counter, JSON.stringify(this.priceArticleCreateDevisCredentials));
    this.router.navigate(['/devis-recap']);
  }

}
