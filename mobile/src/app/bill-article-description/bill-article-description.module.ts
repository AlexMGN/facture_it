import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillArticleDescriptionPageRoutingModule } from './bill-article-description-routing.module';

import { BillArticleDescriptionPage } from './bill-article-description.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillArticleDescriptionPageRoutingModule
  ],
  declarations: [BillArticleDescriptionPage]
})
export class BillArticleDescriptionPageModule {}
