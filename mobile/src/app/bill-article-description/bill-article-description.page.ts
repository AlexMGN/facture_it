import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bill-article-description',
  templateUrl: './bill-article-description.page.html',
  styleUrls: ['./bill-article-description.page.scss'],
})
export class BillArticleDescriptionPage implements OnInit {

  client: any = [];
  counter: any;

  descriptionArticleCreateBillCredentials = {
    description: ''
  };

  constructor(private router: Router) { }

  ngOnInit() {
    if (history.state.description) {
      this.descriptionArticleCreateBillCredentials = history.state.description;
    }
    this.client = JSON.parse(localStorage.getItem('client'));
    this.counter = parseInt(localStorage.getItem('bill_counter'), 10);
  }

  goBack() {
    const articleName = localStorage.getItem('bill_name_article_' + this.counter);
    this.router.navigate(['/bill-article'], {
      state: {
        articleName: JSON.parse(articleName)
      }
    });
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateBill() {
    localStorage.setItem('bill_description_article_' + this.counter, JSON.stringify(this.descriptionArticleCreateBillCredentials));
    this.router.navigate(['/bill-article-price']);
  }

}
