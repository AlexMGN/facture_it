import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillArticleDescriptionPage } from './bill-article-description.page';

const routes: Routes = [
  {
    path: '',
    component: BillArticleDescriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillArticleDescriptionPageRoutingModule {}
