import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';
import auth = firebase.auth;

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.page.html',
  styleUrls: ['./login-page.page.scss'],
})
export class LoginPagePage implements OnInit {

  logCredentials = { email: '', password: '' };
  google: any;

  constructor(private router: Router, public afAuth: AngularFireAuth) { }

  ngOnInit() {
  }

  register() {
    this.router.navigate(['/register-page']);
  }

  login() {
    // console.log(this.logCredentials)

    // Penser à vérifier les input vide etc..
    this.router.navigate(['/me/home/professionnel']);
  }

  forgot() {
    this.router.navigate(['/forgot-page']);
  }

  async googleAuth() {
    await this.GoogleAuth().then(data => {
      this.google = data;
    }).catch(() => {
      // this.toast.sendErrorToast('Une erreur est survenue');
    });

    console.log(this.google.userInfos);
    localStorage.setItem('access_token', '0fef2efzf43fzfrzrf');
    this.router.navigate(['/me/home/professionnel']);
    // Envoyer les DATAS a l'API
  }

  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  AuthLogin(provider) {
    return this.afAuth.signInWithPopup(provider)
        .then((result) => {
          return {
            userInfos: result.additionalUserInfo.profile,
            token: result.credential
          };
        }).catch((error) => {
          console.log(error);
        });
  }

}
