import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BillArticlePage } from './bill-article.page';

describe('BillArticlePage', () => {
  let component: BillArticlePage;
  let fixture: ComponentFixture<BillArticlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillArticlePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BillArticlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
