import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillArticlePageRoutingModule } from './bill-article-routing.module';

import { BillArticlePage } from './bill-article.page';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    BillArticlePageRoutingModule
  ],
  declarations: [BillArticlePage]
})
export class BillArticlePageModule {}
