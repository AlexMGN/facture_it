import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bill-article',
  templateUrl: './bill-article.page.html',
  styleUrls: ['./bill-article.page.scss'],
})
export class BillArticlePage implements OnInit {

  client: any = [];

  nameArticleCreateBillCredentials = {
    name: ''
  };

  constructor(private router: Router) { }

  ngOnInit() {
    if (history.state.articleName) {
      this.nameArticleCreateBillCredentials.name = history.state.articleName;
    }
    this.client = JSON.parse(localStorage.getItem('client'));
  }

  goBack() {
    this.router.navigate(['/create-bills']);
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateBill() {
    const counter = parseInt(localStorage.getItem('bill_counter'), 10);
    localStorage.setItem('bill_name_article_' + counter, JSON.stringify(this.nameArticleCreateBillCredentials));
    this.router.navigate(['/bill-article-description']);
  }

}
