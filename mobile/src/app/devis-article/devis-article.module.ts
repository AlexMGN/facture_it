import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisArticlePageRoutingModule } from './devis-article-routing.module';

import { DevisArticlePage } from './devis-article.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevisArticlePageRoutingModule
  ],
  declarations: [DevisArticlePage]
})
export class DevisArticlePageModule {}
