import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisArticlePage } from './devis-article.page';

const routes: Routes = [
  {
    path: '',
    component: DevisArticlePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisArticlePageRoutingModule {}
