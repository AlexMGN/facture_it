import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devis-article',
  templateUrl: './devis-article.page.html',
  styleUrls: ['./devis-article.page.scss'],
})
export class DevisArticlePage implements OnInit {

  client: any = [];

  nameArticleCreateDevisCredentials = {
    name: ''
  };

  constructor(private router: Router) { }

  ngOnInit() {
    if (history.state.articleName) {
      this.nameArticleCreateDevisCredentials.name = history.state.articleName;
    }
    this.client = JSON.parse(localStorage.getItem('client'));
  }

  goBack() {
    this.router.navigate(['/create-devis']);
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateDevis() {
    const counter = parseInt(localStorage.getItem('devis_counter'), 10);
    localStorage.setItem('devis_name_article_' + counter, JSON.stringify(this.nameArticleCreateDevisCredentials));
    this.router.navigate(['/devis-article-description']);
  }

}
