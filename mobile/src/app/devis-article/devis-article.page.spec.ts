import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisArticlePage } from './devis-article.page';

describe('DevisArticlePage', () => {
  let component: DevisArticlePage;
  let fixture: ComponentFixture<DevisArticlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisArticlePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisArticlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
