import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil-page',
  templateUrl: './profil-page.page.html',
  styleUrls: ['./profil-page.page.scss'],
})
export class ProfilPagePage implements OnInit {

  service: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goBack() {
    if (history.state.service === 'professionnel') {
      this.router.navigate(['/me/home/professionnel']);
    } else if (history.state.service === 'particulier') {
      this.router.navigate(['/me/home/particulier']);
    } else if (history.state.service === 'add-client') {
      this.router.navigate(['/add-client']);
    } else if (history.state.service === 'ficheFactures') {
      this.router.navigate(['/fiche/client/factures'], {
        state: {
          client: history.state.client
        }
      });
    } else if (history.state.service === 'ficheDevis') {
      this.router.navigate(['/fiche/client/devis'], {
        state: {
          client: history.state.client
        }
      });
    } else {
      this.router.navigate(['/me/home/professionnel']);
    }
  }

}
