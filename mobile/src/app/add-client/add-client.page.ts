import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.page.html',
  styleUrls: ['./add-client.page.scss'],
})
export class AddClientPage implements OnInit {

  countries: any;
  addClientCredentials = {
    status: 'Professionnel',
    name: '',
    email: '',
    adresse: '',
    ville: '',
    code_postal: '',
    country: 'France',
    portable: '',
    fixe: ''
  };

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.getCountries();
  }

  getCountries() {
    this.http.get('https://restcountries.eu/rest/v2/all').subscribe((result) => {
      this.countries = result
    })
  }

  goBack() {
    if (history.state.service === 'professionnel') {
      this.router.navigate(['/me/home/professionnel']);
    } else {
      this.router.navigate(['/me/home/particulier']);
    }
  }

  goToProfil() {
    this.router.navigate(['/profil-page'], {
      state: {
        service: 'add-client'
      }
    });
  }

  addClient() {
    console.log(this.addClientCredentials);
  }

}
