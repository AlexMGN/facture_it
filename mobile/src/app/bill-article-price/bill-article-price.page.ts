import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bill-article-price',
  templateUrl: './bill-article-price.page.html',
  styleUrls: ['./bill-article-price.page.scss'],
})
export class BillArticlePricePage implements OnInit {

  client: any = [];
  counter: any;

  priceArticleCreateBillCredentials = {
    quantity: '',
    price: ''
  };

  constructor(private router: Router) { }

  ngOnInit() {
    this.client = JSON.parse(localStorage.getItem('client'));
    this.counter = parseInt(localStorage.getItem('bill_counter'), 10);
  }

  goBack() {
    const articleDescription = localStorage.getItem('bill_name_article_' + this.counter);
    this.router.navigate(['/bill-article-description'], {
      state: {
        description: JSON.parse(articleDescription)
      }
    });
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateBill() {
    localStorage.setItem('bill_description_price_' + this.counter, JSON.stringify(this.priceArticleCreateBillCredentials));
    this.router.navigate(['/bill-recap']);
  }

}
