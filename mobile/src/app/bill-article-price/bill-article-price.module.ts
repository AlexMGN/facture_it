import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillArticlePricePageRoutingModule } from './bill-article-price-routing.module';

import { BillArticlePricePage } from './bill-article-price.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillArticlePricePageRoutingModule
  ],
  declarations: [BillArticlePricePage]
})
export class BillArticlePricePageModule {}
