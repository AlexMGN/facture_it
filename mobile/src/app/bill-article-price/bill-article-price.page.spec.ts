import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BillArticlePricePage } from './bill-article-price.page';

describe('BillArticlePricePage', () => {
  let component: BillArticlePricePage;
  let fixture: ComponentFixture<BillArticlePricePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillArticlePricePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BillArticlePricePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
