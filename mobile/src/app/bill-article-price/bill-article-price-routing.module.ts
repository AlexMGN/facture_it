import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillArticlePricePage } from './bill-article-price.page';

const routes: Routes = [
  {
    path: '',
    component: BillArticlePricePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillArticlePricePageRoutingModule {}
