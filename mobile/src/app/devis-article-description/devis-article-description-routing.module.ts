import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisArticleDescriptionPage } from './devis-article-description.page';

const routes: Routes = [
  {
    path: '',
    component: DevisArticleDescriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisArticleDescriptionPageRoutingModule {}
