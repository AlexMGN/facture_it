import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devis-article-description',
  templateUrl: './devis-article-description.page.html',
  styleUrls: ['./devis-article-description.page.scss'],
})
export class DevisArticleDescriptionPage implements OnInit {

  client: any = [];
  counter: any;

  descriptionArticleCreateDevisCredentials = {
    description: ''
  };

  constructor(private router: Router) { }

  ngOnInit() {
    if (history.state.description) {
      this.descriptionArticleCreateDevisCredentials = history.state.description;
    }
    this.client = JSON.parse(localStorage.getItem('client'));
    this.counter = parseInt(localStorage.getItem('devis_counter'), 10);
  }

  goBack() {
    const articleName = localStorage.getItem('devis_name_article_' + this.counter);
    this.router.navigate(['/devis-article'], {
      state: {
        articleName: JSON.parse(articleName)
      }
    });
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateDevis() {
    localStorage.setItem('devis_description_article_' + this.counter, JSON.stringify(this.descriptionArticleCreateDevisCredentials));
    this.router.navigate(['/devis-article-price']);
  }

}
