import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateDevisPageRoutingModule } from './create-devis-routing.module';

import { CreateDevisPage } from './create-devis.page';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    CreateDevisPageRoutingModule
  ],
  declarations: [CreateDevisPage]
})
export class CreateDevisPageModule {}
