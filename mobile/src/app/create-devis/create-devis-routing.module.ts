import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateDevisPage } from './create-devis.page';

const routes: Routes = [
  {
    path: '',
    component: CreateDevisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateDevisPageRoutingModule {}
