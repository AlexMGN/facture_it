import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateDevisPage } from './create-devis.page';

describe('CreateDevisPage', () => {
  let component: CreateDevisPage;
  let fixture: ComponentFixture<CreateDevisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDevisPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateDevisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
