import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-devis',
  templateUrl: './create-devis.page.html',
  styleUrls: ['./create-devis.page.scss'],
})
export class CreateDevisPage implements OnInit {

  client: any = [];
  countries: any;

  clientInformationsCreateDevisCredentials = {
    email: '',
    adresse: '',
    ville: '',
    code_postal: '',
    country: 'France',
    portable: ''
  };

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.client = JSON.parse(localStorage.getItem('client'));
    this.getCountries();
    this.clientInformationsCreateDevisCredentials = {
      email: this.client.email,
      adresse: this.client.adresse,
      ville: this.client.ville,
      code_postal: this.client.code_postal,
      country: 'France',
      portable: this.client.portable
    };
  }

  getCountries() {
    this.http.get('https://restcountries.eu/rest/v2/all').subscribe((result) => {
      this.countries = result;
    });
  }

  goBack() {
    this.router.navigate(['/me/home/professionnel']);
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateDevis() {
    localStorage.setItem('devis_counter', '1');
    localStorage.setItem('client_informations_devis', JSON.stringify(this.clientInformationsCreateDevisCredentials));
    this.router.navigate(['/devis-article']);
  }

}
