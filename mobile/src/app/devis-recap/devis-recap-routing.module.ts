import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisRecapPage } from './devis-recap.page';

const routes: Routes = [
  {
    path: '',
    component: DevisRecapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisRecapPageRoutingModule {}
