import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devis-recap',
  templateUrl: './devis-recap.page.html',
  styleUrls: ['./devis-recap.page.scss'],
})
export class DevisRecapPage implements OnInit {

  client: any = [];
  counter: any;
  devis: any = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.client = JSON.parse(localStorage.getItem('client'));
    this.counter = parseInt(localStorage.getItem('devis_counter'), 10);
    this.getDevis();
  }

  goBack() {
    const articlePrice = localStorage.getItem('devis_description_price_' + this.counter);
    this.router.navigate(['/devis-article-price'], {
      state: {
        articleName: JSON.parse(articlePrice)
      }
    });
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateDevis() {
    const billCounter = this.counter += 1;
    localStorage.setItem('devis_counter', JSON.stringify(billCounter));
    this.router.navigate(['/devis-article'], {
      state: {
        articleName: ''
      }
    });
  }

  sendDevisToClient() {
    const devis = {
      client: this.client,
      devis: this.devis
    };

    console.log(devis);

    for (let i = 0; i < this.counter; i++) {
      localStorage.removeItem('devis_counter');
      localStorage.removeItem('devis_name_article_' + (i + 1));
      localStorage.removeItem('devis_description_price_' + (i + 1));
      localStorage.removeItem('devis_description_article_' + (i + 1));
      localStorage.removeItem('client_informations_devis' + (i + 1));
    }
    // Afficher une modal pour prévenir le client comme la maquette.
    // S'il accepte, envoyer a l'api sur la route de création de facture le tableau this.bill
  }

  getDevis() {
    for (let i = 0; i < this.counter; i++) {
      this.devis.push({
        devisArticleName: JSON.parse(localStorage.getItem('devis_name_article_' + (i + 1))),
        devisArticleDescription: JSON.parse(localStorage.getItem('devis_description_article_' + (i + 1))),
        devisArticlePrice: JSON.parse(localStorage.getItem('devis_description_price_' + (i + 1))),
      });
    }
  }

  getRecap() {
    // cette fonction sera appelée dans le ngOnInit. Il faudra envoyer this.bill afin de générer un pdf. Une fois reçu, il faudra l'afficher
  }
}
