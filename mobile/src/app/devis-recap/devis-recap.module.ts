import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisRecapPageRoutingModule } from './devis-recap-routing.module';

import { DevisRecapPage } from './devis-recap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevisRecapPageRoutingModule
  ],
  declarations: [DevisRecapPage]
})
export class DevisRecapPageModule {}
