import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisRecapPage } from './devis-recap.page';

describe('DevisRecapPage', () => {
  let component: DevisRecapPage;
  let fixture: ComponentFixture<DevisRecapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisRecapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisRecapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
