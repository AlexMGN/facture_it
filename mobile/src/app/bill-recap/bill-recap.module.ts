import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillRecapPageRoutingModule } from './bill-recap-routing.module';

import { BillRecapPage } from './bill-recap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillRecapPageRoutingModule
  ],
  declarations: [BillRecapPage]
})
export class BillRecapPageModule {}
