import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BillRecapPage } from './bill-recap.page';

describe('BillRecapPage', () => {
  let component: BillRecapPage;
  let fixture: ComponentFixture<BillRecapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillRecapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BillRecapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
