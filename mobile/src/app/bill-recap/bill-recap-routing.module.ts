import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillRecapPage } from './bill-recap.page';

const routes: Routes = [
  {
    path: '',
    component: BillRecapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillRecapPageRoutingModule {}
