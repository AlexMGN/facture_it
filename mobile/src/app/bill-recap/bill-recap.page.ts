import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bill-recap',
  templateUrl: './bill-recap.page.html',
  styleUrls: ['./bill-recap.page.scss'],
})
export class BillRecapPage implements OnInit {

  client: any = [];
  counter: any;
  bill: any = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.client = JSON.parse(localStorage.getItem('client'));
    this.counter = parseInt(localStorage.getItem('bill_counter'), 10);
    this.getBill();
  }

  goBack() {
    const articlePrice = localStorage.getItem('bill_description_price_' + this.counter);
    this.router.navigate(['/bill-article-price'], {
      state: {
        articleName: JSON.parse(articlePrice)
      }
    });
  }

  goToProfil() {
    this.router.navigate(['/profil-page']);
  }

  continueToCreateBill() {
    const billCounter = this.counter += 1;
    localStorage.setItem('bill_counter', JSON.stringify(billCounter));
    this.router.navigate(['/bill-article'], {
      state: {
        articleName: ''
      }
    });
  }

  sendBillToClient() {
    const bill = {
      client: this.client,
      bill: this.bill
    };

    console.log(bill);

    for (let i = 0; i < this.counter; i++) {
      localStorage.removeItem('bill_counter');
      localStorage.removeItem('bill_name_article_' + (i + 1));
      localStorage.removeItem('bill_description_price_' + (i + 1));
      localStorage.removeItem('bill_description_article_' + (i + 1));
      localStorage.removeItem('client_informations_bill' + (i + 1));
    }
    // Afficher une modal pour prévenir le client comme la maquette.
    // S'il accepte, envoyer a l'api sur la route de création de facture le tableau this.bill
  }

  getBill() {
    for (let i = 0; i < this.counter; i++) {
      this.bill.push({
        billArticleName: JSON.parse(localStorage.getItem('bill_name_article_' + (i + 1))),
        billArticleDescription: JSON.parse(localStorage.getItem('bill_description_article_' + (i + 1))),
        billArticlePrice: JSON.parse(localStorage.getItem('bill_description_price_' + (i + 1))),
      });
    }
  }

  getRecap() {
    // cette fonction sera appelée dans le ngOnInit. Il faudra envoyer this.bill afin de générer un pdf. Une fois reçu, il faudra l'afficher
  }

}
